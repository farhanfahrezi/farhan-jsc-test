import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import './uikit.min.css';

import ContactList from './layouts/ContactList';
import Header from './layouts/Header';
import ContactModalSelect from './layouts/ContactModalSelect';

function App() {
  const [ contacts, setContacts ] = useState(JSON.parse(localStorage.getItem('contacts')) || []);
  const [ showModal, setShowModal ] = useState(false);

  return (
    <>
      <div className="App">
        <Header onAddClick={() => setShowModal(true)} />
        <ContactList key={contacts} data={[...contacts]} />
        { showModal && <ContactModalSelect closeModal={() => setShowModal(false)} refreshData={(data) => setContacts(data)} /> }
      </div>
    </>
  );
}

export default App;
