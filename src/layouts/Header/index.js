import React from 'react';
import './style.css';

function Header(props) {
  return (
    <>
      <div className="header-container">
        <span>Kelola Kontak</span>

        <button className="uk-button uk-button-primary" onClick={() => props.onAddClick()}>
          <ul className="uk-iconnav">
            <li><a href="#" uk-icon="icon: plus"></a></li>
          </ul>
          Tambah Kontak
        </button>
      </div>
    </>
  );
}

export default Header;
