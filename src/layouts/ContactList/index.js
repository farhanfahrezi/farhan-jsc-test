import React, { useState, useEffect } from 'react';
import './style.css';

function ContactList(props) {
  const [ data, setData ] = useState(props.data);
  const [ selectedIndex, setSelectedIbdex ] = useState(0);
  const [ showModal, setShowModal ] = useState(false);

  const show = (index) => {
    setSelectedIbdex(index);
    setShowModal(true);
  }

  const deleteContact = () => {
    let _data = [...data];
    
    _data.splice(selectedIndex, 1);
    localStorage.setItem('contacts', JSON.stringify(_data))
    setData(_data)
    setShowModal(false)
  }

  const _renderList = () => {
    let items = [];

    data.forEach((item, index) => {
      items.push(
        <div key={item.email} className="contact-item" onClick={() => show(index)}>
          <img src={item.picture.thumbnail} />
          <span>{item.name.first} {item.name.last}</span>
        </div>
      )
    })

    return items
  }

  const jenisKelamin = (gender) => {
    switch (gender) {
      case 'male':
        return 'Laki-Laki'
        break;
      case 'female':
        return 'Perempuan'
        break;
    
      default:
        break;
    }
  }

  return (
    <div className="contact-wrapper">
      <div className="uk-container">
        { _renderList() }
      </div>

      {
        showModal &&
          <div id="contact-modal-sections" className="uk-modal uk-open" style={{ display: 'block' }}>
            <div className="uk-modal-dialog modal">
              <div className="uk-modal-header">
                  <h2 className="uk-modal-title">Kontak Detail</h2>
              </div>
              <div>
                <img className="profile-pic" src={data[selectedIndex].picture.large} />
                <p>{data[selectedIndex].name.first} {data[selectedIndex].name.last}</p>

                <div className="row">
                  <span>Lahir</span>
                  <p>{ data[selectedIndex].dob.date }</p>
                </div>
                <div className="row">
                  <span>Jenis Kelamin</span>
                  <p>{ jenisKelamin(data[selectedIndex].gender) }</p>
                </div>

                <div className="row">
                  <span>Surel</span>
                  <p>{ data[selectedIndex].email }</p>
                </div>
                <div className="row">
                  <span>Telepon</span>
                  <p>{ data[selectedIndex].phone }</p>
                </div>

                <div className="row">
                  <span>Tempat Tinggal</span>
                  <p>
                    {
                      `
                        ${data[selectedIndex].location.street.number}
                        ${data[selectedIndex].location.street.name}
                        ${data[selectedIndex].location.city },
                        ${data[selectedIndex].location.state },
                        ${data[selectedIndex].location.country },
                        ${data[selectedIndex].location.postcode }
                      `
                    }
                  </p>
                </div>
                <div className="row">
                  <span>Koordinat</span>
                  <p>{ data[selectedIndex].location.coordinates.latitude }, { data[selectedIndex].location.coordinates.longitude }</p>
                </div>


              </div>
              <div className="uk-modal-footer uk-text">
                <button className="uk-button uk-button-default uk-modal-close" type="button" onClick={() => setShowModal(false)}>Close</button>
                <button className="uk-button uk-button-danger" type="button" onClick={() => deleteContact()}>Hapus</button>
              </div>
            </div>
          </div>
      }
    </div>
  );
}

export default ContactList;
