import React, { useState, useEffect } from 'react';
import './style.css';
import Axios from 'axios';

function ContactModalSelect(props) {
  const [ data, setData] = useState([]);
  const [ loading, setLoading] = useState(true);
  
  useEffect(() => {
    Axios.get('https://randomuser.me/api?results=5&exc=login,registered,id,nat&nat=us&noinfo')
      .then((res) => {
        if (res?.data){
          setData(res.data.results)
        }
      })
      .catch(error => {
        return false;
      })
      .finally(() => {
        setLoading(false);
      })
  }, [])

  const addContact = async (item) => {
    let _data = JSON.parse(localStorage.getItem('contacts')) || [];
    let newData = [ ..._data, item ];

    localStorage.setItem('contacts', JSON.stringify(newData));

    props.refreshData(newData)
    props.closeModal()
  }
  
  const _renderList = () => {
    let items = [];

    data.forEach((item, index) => {
      items.push(
        <div key={item.email} className="contact-item" onClick={() => addContact(item)}>
          <img src={item.picture.thumbnail} />
          <span>{item.name.first} {item.name.last}</span>
        </div>
      )
    })

    return items
  }

  return (
    <>
      <script src="js/uikit.min.js"></script>
      <script src="js/uikit-icons.min.js"></script>

      <div id="contact-select-sections" className="uk-modal uk-open" style={{ display: 'block' }}>
        <div className="uk-modal-dialog">
          <div className="uk-modal-header">
              <h2 className="uk-modal-title">Pilih Kontak</h2>
          </div>
          <div className="uk-modal-body">
            { loading ? <div uk-spinner="true" /> : _renderList() }
          </div>
          <div className="uk-modal-footer uk-text">
            <button className="uk-button uk-button-default uk-modal-close" type="button" onClick={() => props.closeModal()}>Cancel</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default ContactModalSelect;
